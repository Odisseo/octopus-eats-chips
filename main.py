from math import factorial
import sys
seq=""
counter=0
twoCount=0
oneCount=0
verbose=True

def countPermutationsWORep(seq):
	global verbose,twoCount,oneCount
	coeffMultinomiale = int(factorial(len(seq)))//int((factorial(oneCount)*factorial(twoCount)))
	if verbose:
		print("-> "+str(coeffMultinomiale))
	print("Manca "+str(twoCount)+" 2")
	return coeffMultinomiale

def iterate():
	global seq,counter,twoCount,oneCount
	while "2" in seq:
		seq = seq[::-1].replace("2","11",1)
		seq=seq[::-1]
		twoCount-=1
		oneCount+=2
		if verbose:
			print(seq,end="")
		counter +=countPermutationsWORep(seq)

def start(n):
	global seq,counter,verbose,twoCount,oneCount
	for i in range(0,int(n/2)):
		seq+="2"
		twoCount+=1
	if verbose:
		print(seq+" -> 1")
	counter +=1
	iterate()



		
if __name__=="__main__":
	n=6
	#verbose = True
	start(n)
	print("result: "+str(counter))